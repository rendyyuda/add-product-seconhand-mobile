package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import ch.qos.logback.core.joran.conditional.ElseAction
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import groovy.json.StringEscapeUtils
import internal.GlobalVariable

public class AddProduct {
	@When("User tap Jual button")
	public void user_tap_Jual_button() {
		Mobile.tap(findTestObject('Tambah_Produk/button_addproduct'), 0)
		Mobile.verifyElementVisible(findTestObject('Tambah_Produk/button_Terbitkan'), 0)
	}

	@When("User input product name with {string}")
	public void user_input_product_name_with(String name) {
		Mobile.setText(findTestObject('Tambah_Produk/input_NamaProduk'), name, 0)
	}

	@When("User input product price with {string}")
	public void user_input_product_price_with(String price) {
		Mobile.setText(findTestObject('Tambah_Produk/input_HargaProduk'), price, 0)
	}

	@When("User choose product category")
	public void user_choose_product_category() {
		Mobile.tap(findTestObject('Tambah_Produk/select_Kategori'), 0)
		Mobile.delay(7)
		Mobile.tap(findTestObject('Tambah_Produk/category_Handphone'), 0)
	}

	@When("User input product location with {string}")
	public void user_input_product_location_with(String location) {
		Mobile.setText(findTestObject('Tambah_Produk/input_Lokasi'), location, 0)
	}

	@When("User input product description with {string}")
	public void user_input_product_description_with(String desc) {
		Mobile.setText(findTestObject('Tambah_Produk/textarea_Deskripsi'), desc, 0)
	}

	@When("User input product image")
	public void user_input_product_image() {
		Mobile.tap(findTestObject('Tambah_Produk/upload_Image'), 0)
		Mobile.tap(findTestObject('Tambah_Produk/choose_Galeri'), 0)
		Mobile.tap(findTestObject('Tambah_Produk/foto1'), 0)
	}

	@When("User tap terbitkan button")
	public void user_tap_terbitkan_button() {
		Mobile.tap(findTestObject('Tambah_Produk/button_Terbitkan'), 0)
	}

	@When("User see message of (.*)")
	public void user_see_message_of(String message) {
		if (message=='successfully') {
			Mobile.verifyElementVisible(findTestObject('Tambah_Produk/message_SuksesTambahProduk'), 0)
		}else if(message=='name empty') {
			Mobile.verifyElementVisible(findTestObject('Tambah_Produk/message_NameEmpty'), 0)
		}else if(message=='price empty') {
			Mobile.verifyElementVisible(findTestObject('Tambah_Produk/message_PriceEmpty'), 0)
		}else if(message=='location empty') {
			Mobile.verifyElementVisible(findTestObject('Tambah_Produk/message_LocationEmpty'), 0)
		}else if(message=='description empty') {
			Mobile.verifyElementVisible(findTestObject('Tambah_Produk/message_DescriptionEmpty'), 0)
		}
	}

	@Then("User successfully add product")
	public void user_successfully_add_product() {
		Mobile.verifyElementVisible(findTestObject('Tambah_Produk/message_SuksesTambahProduk'), 0)
		Mobile.verifyElementVisible(findTestObject('DaftarJual_page/text_DaftarJualSaya'), 0)
	}

	@Then("User failed add product")
	public void user_failed_add_product() {
		Mobile.verifyElementVisible(findTestObject('Tambah_Produk/text_TambahProduk'), 0)
	}
}